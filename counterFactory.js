function counterFactory() {

    let counter = 100;
    return {
        increment(){
            return counter + 1;
        },
        decrement(){
            return counter - 1;
        }
    };
};


module.exports = { counterFactory };
