const cacheFunctionFile = require('../cacheFunction')

const testerFunction = (para1, para2) => {

    return para1 + para2;
}

let tempVar = cacheFunctionFile.cacheFunction(testerFunction);
console.log(tempVar(5, 4))
console.log(tempVar(2, 2))
console.log(tempVar(1, 3))
console.log(tempVar(5, 4))

