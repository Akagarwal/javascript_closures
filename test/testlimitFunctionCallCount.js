const importFunction = require('../limitFunctionCallCount')

function testCb(){
    console.log('Hello World')
}

const testFunction = importFunction.limitFunctionCallCount(testCb, 3);

testFunction();
testFunction();
testFunction();
//prints three times and then stops(as n=3)
testFunction();
testFunction();

